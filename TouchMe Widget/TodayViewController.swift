//
//  TodayViewController.swift
//  TouchMe Widget
//
//  Created by Radislav Crechet on 5/26/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit
import NotificationCenter
import TouchMeKit

class TodayViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet var avatarLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var positionLabel: UILabel!
    
    // MARK: Properties
    
    //fileprivate var gamer: [String: Any]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureGamer()
    }
    
    // MARK: Configuration
    
    fileprivate func configureGamer() {
        guard SessionService.haveSession else {
            return
        }
        
        let gamer = DatabaseManager.shared.gamer()!
        avatarLabel.text = String(gamer.email.characters.first!)
        emailLabel.text = gamer.email
        scoreLabel.text = String(gamer.score)
        positionLabel.text = String(gamer.position)
        
        updateLeaderboard { [unowned self] in
            let gamer = DatabaseManager.shared.gamer()!
            let position = gamer.position
            self.scoreLabel.text = String(gamer.score)
            self.positionLabel.text = position > 0 ? String(position) : "-"
        }
    }
    
    // MARK: Actions
    
    @IBAction func viewPressed(_ sender: Any) {
        extensionContext!.open(SchemeService.profileUrl, completionHandler: nil)
    }
    
}

// MARK: -

extension TodayViewController {
    
    // MARK: Work With Leaderboard
    
    fileprivate func updateLeaderboard(_ completion: @escaping () -> Void) {
        NetworkService.leaderboard {
            completion()
        }
    }
    
}
