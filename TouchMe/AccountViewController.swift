//
//  AccountViewController.swift
//  TouchMe
//
//  Created by Radislav Crechet on 5/22/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit
import Hero
import TouchMeKit

class AccountViewController: ViewController {

    // MARK: Outlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    // MARK: Properties
    
    fileprivate var isEnteredDataValid: Bool {
        var isEnteredDataValid = false
        
        if let email = emailTextField.text,
            let password = passwordTextField.text,
            !email.isEmpty &&
                !password.isEmpty {
            
            isEnteredDataValid = true
        }
        
        return isEnteredDataValid
    }
    
    // MARK: Presentation
    
    fileprivate func resignFirstResponders() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    // MARK: Actions
    
    @IBAction func createButtonPressed(_ sender: UIButton) {
        createAccount()
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        Hero.shared.setDefaultAnimationForNextTransition(.fade)
        navigationController?.popViewController(animated: true)
    }
    
}

// MARK: -

extension AccountViewController {
    
    // MARK: Work With Account
    
    fileprivate func createAccount() {
        if isEnteredDataValid {
            resignFirstResponders()
            createKeychainPasswordItem()
            createSession()
            createGamerInDatabase()
            signUpWithFirebase()
        }
    }
    
}

// MARK: -

extension AccountViewController {
    
    // MARK: Work With Keychain
    
    fileprivate func createKeychainPasswordItem() {
        let item = KeychainPasswordItem(service: "TouchMe", account: emailTextField.text!)
        try! item.savePassword(passwordTextField.text!)
    }
    
}

// MARK: -

extension AccountViewController {
    
    // MARK: Work With Session
    
    fileprivate func createSession() {
        SessionService.setEmail(emailTextField.text!)
    }
    
}

// MARK: -

extension AccountViewController {
    
    // MARK: Work With Database
    
    fileprivate func createGamerInDatabase() {
        let gamer = Gamer()
        gamer.email = emailTextField.text!
        
        DatabaseManager.shared.add(gamer)
    }
    
}

// MARK: -

extension AccountViewController {
    
    // MARK: Work With Firebase
    
    fileprivate func signUpWithFirebase() {
        ActivityView.show() { [unowned self] in
            FirebaseService.signUp(withEmail: self.emailTextField.text!,
                                   password: self.passwordTextField.text!) { [unowned self] _ in
                                    
                                    ActivityView.hide() { [unowned self] in
                                        self.presentMain()
                                    }
            }
        }
    }
    
}

// MARK: -

extension AccountViewController: UITextFieldDelegate {
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            createAccount()
        }
        
        return true
    }
    
}
