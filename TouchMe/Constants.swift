//
//  Constants.swift
//  TouchMe
//
//  Created by Radislav Crechet on 5/22/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import Foundation

struct Constants {

    struct StoryboardNames {
        
        // MARK: Properties
        
        static let main = "Main"
        
    }
    
    struct SegueIdentifiers {
        
        // MARK: Properties
        
        static let toAccout = "ToAccount"
        static let toProfile = "ToProfile"
        
    }
    
    struct ViewControllerIdentifiers {
        
        // MARK: Properties
        
        static let profileViewController = "ProfileViewController"
        
    }
    
}
