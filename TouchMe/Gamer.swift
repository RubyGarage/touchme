//
//  Gamer.swift
//  TouchMe
//
//  Created by Radislav Crechet on 5/26/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import RealmSwift

public class Gamer: Object {

    // MARK: Properties
    
    public dynamic var email = ""
    public dynamic var score = 0
    public dynamic var position = 0

}
