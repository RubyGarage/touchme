//
//  ViewController.swift
//  TouchMe
//
//  Created by Radislav Crechet on 5/22/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit
import LocalAuthentication
import Hero
import TouchMeKit

class EntryViewController: ViewController {
    
    // MARK: Outlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var orLabel: UILabel!
    @IBOutlet var thouchTheButton: UIButton!
    
    // MARK: Properties
    
    fileprivate var isEnteredDataValid: Bool {
        var isEnteredDataValid = false
        
        if let email = emailTextField.text,
            let password = passwordTextField.text,
            !email.isEmpty &&
                !password.isEmpty {
            
            isEnteredDataValid = true
        }
        
        return isEnteredDataValid
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSession()
        configureAuthentication()
    }
    
    // MARK: Configuration
    
    fileprivate func configureSession() {
        guard SessionService.haveSession else {
            return
        }
        
        emailTextField.text = SessionService.email
        
        if AuthenticationService.canAuthenticateWithBiometrics {
            authenticateWithBiometrics()
        }
    }
    
    private func configureAuthentication() {
        if !AuthenticationService.canAuthenticateWithBiometrics {
            orLabel.isHidden = true
            thouchTheButton.isHidden = true
        }
    }
    
    // MARK: Presentation
    
    fileprivate func resignFirstResponders() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    // MARK: Actions

    @IBAction func enterWithAccountButtonPressed(_ sender: UIButton) {
        enterWithAccount()
    }
    
    @IBAction func touchTheButtonPressed(_ sender: Any) {
        authenticateWithBiometrics()
    }
    
    @IBAction func firstTimeButtonPressed(_ sender: Any) {
        Hero.shared.setDefaultAnimationForNextTransition(.fade)
        performSegue(withIdentifier: Constants.SegueIdentifiers.toAccout, sender: self)
    }

}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Authentication
    
    fileprivate func authenticateWithBiometrics() {
        guard let email = emailTextField.text,
            !email.isEmpty else {
                
                return
        }
        
        resignFirstResponders()
        
        guard email == SessionService.email else {
            presentErrorAlert(withMessage: "You trying authorize with another email. Please, enter the password.")
            return
        }
        
        AuthenticationService.authenticateWithBiometrics(withLocalizedReason: "to enter") { [unowned self] success,
            policyError, authenticationError in
            
            guard !success else {
                self.signInWithFirebase()
                
                return
            }
            
            var message = policyError?.localizedDescription
            
            if let error = authenticationError,
                error.code != LAError.userFallback &&
                    error.code != LAError.userCancel &&
                    error.code != LAError.systemCancel {
                
                message = error.localizedDescription
            }
            
            if let message = message {
                self.presentErrorAlert(withMessage: message)
            }
        }
    }
    
}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Account
    
    fileprivate func enterWithAccount() {
        if isEnteredDataValid {
            resignFirstResponders()
            
            if isKeychainPasswordItemValid() {
                signInWithFirebase()
            }
        }
    }
    
}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Keychain
    
    fileprivate func isKeychainPasswordItemValid() -> Bool {
        do {
            
            let item = KeychainPasswordItem(service: "TouchMe", account: emailTextField.text!)
            let password = try item.readPassword()
            
            if password == passwordTextField.text {
                return true
            } else {
                presentErrorAlert(withMessage: "Invalid password")
                return false
            }
            
        } catch {
            
            if let error = error as? KeychainPasswordItem.KeychainError {
                if case .noPassword = error {
                    presentErrorAlert(withMessage: "There is no email-password pair in keychain")
                    return false
                }
            }
            
            presentErrorAlert(withMessage: error.localizedDescription)
            
        }
        
        return false
    }
    
}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Firebase
    
    fileprivate func signInWithFirebase() {
        createSession()
        
        ActivityView.show() { [unowned self] in
            self.createGamerInDatabaseIfNeeded {
                FirebaseService.signIn(withEmail: self.emailTextField.text!,
                                       password: self.passwordTextField.text!) { [unowned self] in
                                        
                                        NetworkService.leaderboard { [unowned self] in
                                            self.updateGamerScoreInDatabase()
                                            
                                            ActivityView.hide() { [unowned self] in
                                                self.presentMain()
                                            }
                                        }
                                        
                }
            }
        }
    }
    
}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Session
    
    fileprivate func createSession() {
        SessionService.setEmail(emailTextField.text!)
    }
    
}

// MARK: -

extension EntryViewController {
    
    // MARK: Work With Database
    
    fileprivate func createGamerInDatabaseIfNeeded(_ completion: @escaping Completion) {
        guard DatabaseManager.shared.gamer() == nil else {
            completion()
            return
        }
        
        let gamer = Gamer()
        gamer.email = emailTextField.text!
        
        DatabaseManager.shared.add(gamer)
        
        completion()
    }
    
    fileprivate func updateGamerScoreInDatabase() {
        let email = SessionService.email!
        let score = LeaderboardManager.shared.scoreOfGamer(withEmail: email)
        DatabaseManager.shared.updateScore(score)
    }
    
}

// MARK: -

extension EntryViewController: UITextFieldDelegate {
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            enterWithAccount()
        }
        
        return true
    }
    
}

