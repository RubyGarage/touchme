//
//  Constants.swift
//  TouchMe
//
//  Created by Radislav Crechet on 5/29/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import Foundation

struct Constants {
    
    struct GroupIdentifiers {
        
        // MARK: Properties
        
        static let touchMe = "group.org.rubygarage.touchme"
        
    }

}
